package modelo.componentes;

public class Resistencia extends Componente {

	public Resistencia() {
		super("Rdef", 100, "ohm");
	}

	public Resistencia(String nombre, float valor) {
		super(nombre, valor, "ohm");
	}

	@Override
	public float calcularImpedancia() {		
		return getValor();
	}
	
	//equals, hashcode y toString sirven los de lpadrer

}
